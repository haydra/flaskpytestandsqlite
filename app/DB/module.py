import sqlite3


def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d


conn = sqlite3.connect('C:\\sqlite\\db.db')
conn.row_factory = dict_factory

Cursor = conn.cursor()


def db_create_new_user(user):
    Cursor.execute("INSERT INTO User VALUES (%s,'%s','%s','%s','%s')" % (
        user["Id"], user["FirstName"], user['LastName'], user['Email'], user['Password']))
    conn.commit()


def db_create_new_task_list(task_list):
    Cursor.execute("INSERT INTO TaskList VALUES (%s,'%s','%s','%s')" % (
        task_list["Id"], task_list["UserId"], task_list['Name'], task_list['Description']))
    conn.commit()


def db_create_new_task(task):
    Cursor.execute("INSERT INTO Task VALUES (%s,'%s','%s','%s','%s','%s')" % (
        task["Id"], task["TaskListId"], task['Name'], task['Description'], task['ScheduledTime'], task['Status']))
    conn.commit()


def update_user(user_id, column_name, new_value):
    Cursor.execute("UPDATE User SET %s='%s' WHERE Id=%s" % (column_name, new_value, user_id
                                                            ))
    conn.commit()
    return get_user_by_id(user_id)


def update_task_list(task_list_id, column_name, new_value):
    Cursor.execute("UPDATE TaskList SET %s='%s' WHERE Id=%s" % (column_name, new_value, task_list_id
                                                                ))
    conn.commit()

    return get_task_list_by_id(task_list_id)


def update_task(task_id, column_name, new_value):
    Cursor.execute("UPDATE Task SET %s='%s' WHERE Id=%s" % (column_name, new_value, task_id
                                                            ))
    conn.commit()

    return get_task_by_id(task_id)


def delete_user(user_id):
    Cursor.execute("DELETE FROM User Where Id=%s" % (user_id
                                                     ))
    conn.commit()


def delete_task_list(task_list_id):
    Cursor.execute("DELETE FROM TaskList Where Id=%s" % (task_list_id
                                                         ))
    conn.commit()


def delete_task(task_id):
    Cursor.execute("DELETE FROM Task Where Id=%s" % (task_id
                                                     ))
    conn.commit()


def get_user_max_id():
    result = Cursor.execute('SELECT MAX(Id) FROM User')
    for row in result:
        return row['MAX(Id)']


def get_tasks_lists_max_id():
    result = Cursor.execute('SELECT MAX(Id) FROM TaskList')
    for row in result:
        return row['MAX(Id)']


def get_tasks_max_id():
    result = Cursor.execute('SELECT MAX(Id) FROM Task')
    for row in result:
        return row['MAX(Id)']


def get_user_by_id(user_id):
    result = Cursor.execute('SELECT * FROM User WHERE Id=%s' % (user_id))
    return result.fetchone()


def get_task_list_by_id(task_list_id):
    result = Cursor.execute('SELECT * FROM TaskList WHERE Id=%s' % (task_list_id))
    return result.fetchone()


def get_task_by_id(task_id):
    result = Cursor.execute('SELECT * FROM Task WHERE Id=%s' % (task_id))
    return result.fetchone()


def get_all_users():
    return Cursor.execute('SELECT * FROM User').fetchall()


def get_all_task_lists():
    return Cursor.execute('SELECT * FROM TaskList').fetchall()


def get_all_tasks():
    return Cursor.execute('SELECT * FROM Task').fetchall()
