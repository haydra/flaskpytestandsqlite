# import ConfigParser
#
# config = ConfigParser.RawConfigParser()
# config.add_section('DB')
# config.set('DB', 'DB_TYPE', 'Files')
#
# with open('configFile.cfg', 'wb') as configfile:
#     config.write(configfile)
import ConfigParser
import os

filePath = os.path.join(os.path.abspath(os.path.dirname(__file__)), "configFile.cfg")
config = ConfigParser.ConfigParser()
config.read(filePath)
