import os
import pickle, cPickle
from flask import json

APP_DIR = 'app'
FILES_DIR = 'psuedoDBfiles'
USERS_FILE_NAME = os.path.join(os.path.abspath(os.path.dirname(__file__)), FILES_DIR, "users.pkl")
TASKS_LISTS_FILE_NAME = os.path.join(os.path.abspath(os.path.dirname(__file__)), FILES_DIR, "tasksListsFile.pkl")
TASKS_FILE_NAME = os.path.join(os.path.abspath(os.path.dirname(__file__)), FILES_DIR, "tasksFile.pkl")


def write_user_to_file(user):
    f = open(USERS_FILE_NAME, "ab")
    pickle.dump(user, f, pickle.HIGHEST_PROTOCOL)
    f.close()


def read_users_to_file():
    f = open(USERS_FILE_NAME, 'a')
    f.close()
    f = open(USERS_FILE_NAME, "rb")
    tempusers = []
    lines = []
    if os.stat(USERS_FILE_NAME).st_size != 0:
        while 1:
            try:
                o = pickle.load(f)
            except EOFError:
                break
            tempusers.append(o)
    return tempusers


def write_task_list_to_file(task_list):
    f = open(TASKS_LISTS_FILE_NAME, "ab")
    pickle.dump(task_list, f, pickle.HIGHEST_PROTOCOL)
    f.close()


def read_tasks_lists_from_file():
    f = open(TASKS_LISTS_FILE_NAME, 'a')
    f.close()
    f = open(TASKS_LISTS_FILE_NAME, "rb")
    temp_tasks_list = []
    lines = []
    if os.stat(TASKS_LISTS_FILE_NAME).st_size != 0:
        while 1:
            try:
                o = pickle.load(f)
            except EOFError:
                break
            temp_tasks_list.append(o)
    return temp_tasks_list


def write_task_to_file(task):
    f = open(TASKS_FILE_NAME, "ab")
    pickle.dump(task, f, pickle.HIGHEST_PROTOCOL)
    f.close()


def read_tasks_from_file():
    f = open(TASKS_FILE_NAME, 'a')
    f.close()
    f = open(TASKS_FILE_NAME, "rb")
    temp_tasks = []
    lines = []
    if os.stat(TASKS_FILE_NAME).st_size != 0:
        while 1:
            try:
                o = pickle.load(f)
            except EOFError:
                break
            temp_tasks.append(o)
    return temp_tasks


def update_user_in_file(var_to_change, new_value, user_id):
    f = open(USERS_FILE_NAME, 'a')
    f.close()
    lines = read_users_to_file()
    open(USERS_FILE_NAME, 'w').close()
    task = None
    for i in range(len(lines)):
        if lines[i]['Id'] == user_id:
            new_json = lines[i]
            new_json[var_to_change] = new_value
            lines[i] = new_json
            task = lines[i]
        write_user_to_file(lines[i])
    return task


def update_task_in_file(var_to_change, new_value, task_id):
    f = open(TASKS_FILE_NAME, 'a')
    f.close()
    lines = read_tasks_from_file()
    open(TASKS_FILE_NAME, 'w').close()
    task = None
    for i in range(len(lines)):
        if lines[i]['Id'] == task_id:
            new_json = lines[i]
            new_json[var_to_change] = new_value
            lines[i] = new_json
            task = lines[i]
        write_task_to_file(lines[i])
    return task


def update_task_list_in_file(var_to_change, new_value, task_list_id):
    f = open(TASKS_LISTS_FILE_NAME, 'a')
    f.close()
    lines = read_tasks_lists_from_file()
    open(TASKS_LISTS_FILE_NAME, 'w').close()
    task = None
    for i in range(len(lines)):
        if lines[i]['Id'] == task_list_id:
            new_json = lines[i]
            new_json[var_to_change] = new_value
            lines[i] = new_json
            task = lines[i]
        write_task_list_to_file(lines[i])
    return task


def delete_user(user_id):
    f = open(USERS_FILE_NAME, 'a')
    f.close()
    lines = read_users_to_file()
    f = open(USERS_FILE_NAME, "w").close()
    line = None
    for i in range(len(lines)):
        if lines[i]['Id'] == user_id:
            line = lines[i]
            lines.remove(lines[i])
            break
    for i in range(len(lines)):
        write_user_to_file(lines[i])
    return line


def delete_task_list(task_list_id):
    f = open(TASKS_LISTS_FILE_NAME, 'a')
    f.close()
    lines = read_tasks_lists_from_file()
    f = open(TASKS_LISTS_FILE_NAME, "w").close()
    line = None
    for i in range(len(lines)):
        if lines[i]['Id'] == task_list_id:
            line = lines[i]
            lines.remove(lines[i])
            break
    for i in range(len(lines)):
        write_task_list_to_file(lines[i])
    return line


def delete_task(task_id):
    f = open(TASKS_FILE_NAME, 'a')
    f.close()
    lines = read_tasks_from_file()
    f = open(TASKS_FILE_NAME, "w").close()
    line = None
    for i in range(len(lines)):
        if lines[i]['Id'] == task_id:
            line = lines[i]
            lines.remove(lines[i])
            break
    for i in range(len(lines)):
        write_task_to_file(lines[i])
    return line

if __name__ == "__main__":
    pickle.dumps(TASKS_LISTS_FILE_NAME)