from flask import Flask, jsonify, make_response
from flask import request
from app import config
import myFileUtils
from DB import module as db_module
import myFileUtils as utils


def create_app():
    app = Flask(__name__)
    config_value = config.get('DB', 'db_type')

    @app.route('/todo/api/user/create', methods=['POST'])
    def create_user():
        if config_value == 'Files':
            _usersList = utils.read_users_to_file()
            user = {
                'Id': _usersList[-1]['Id'] + 1 if len(_usersList) > 0  else 1,
                'FirstName': request.json.get('FirstName', ""),
                'LastName': request.json.get('LastName', ""),
                'Email': request.json.get('Email', ""),
                'Password': request.json.get('Password', "")
            }
            utils.write_user_to_file(user)
            _usersList = utils.read_users_to_file()
        else:
            max_user_id = db_module.get_user_max_id() + 1 if db_module.get_user_max_id() else 1
            user = {
                'Id': max_user_id,
                'FirstName': request.json.get('FirstName', ""),
                'LastName': request.json.get('LastName', ""),
                'Email': request.json.get('Email', ""),
                'Password': request.json.get('Password', "")
            }
            db_module.db_create_new_user(user)
        return jsonify({'user': user}), 201

    @app.route('/todo/api/task-list/create', methods=['POST'])
    def create_task_list():
        if config_value == 'Files':
            _tasksLists = utils.read_tasks_lists_from_file()
            task_list = {
                'Id': _tasksLists[-1]['Id'] + 1 if len(_tasksLists) > 0 else 1,
                'UserId': request.json.get('UserId', ""),
                'Name': request.json.get('Name', ""),
                'Description': request.json.get('Description', "")
            }
            utils.write_task_list_to_file(task_list)
            _tasksLists = utils.read_tasks_lists_from_file()
        else:
            task_list_max_id = db_module.get_tasks_lists_max_id() + 1 if db_module.get_tasks_lists_max_id() != None else 1
            task_list = {
                'Id': task_list_max_id,
                'UserId': request.json.get('UserId', ""),
                'Name': request.json.get('Name', ""),
                'Description': request.json.get('Description', "")
            }
            db_module.db_create_new_task_list(task_list)
        return jsonify({'task_list': task_list}), 201

    @app.route('/todo/api/task/create', methods=['POST'])
    def create_task():
        if config_value == 'Files':
            _tasks = utils.read_tasks_from_file()
            task = {
                'Id': _tasks[-1]['Id'] + 1 if len(_tasks) > 0 else 1,
                'TaskListId': request.json.get('TaskListId', ""),
                'Name': request.json.get('Name', ""),
                'Description': request.json.get('Description', ""),
                'ScheduledTime': request.json.get('ScheduledTime', ""),
                'Status': request.json.get('Status', "")
            }
            utils.write_task_to_file(task)
            _tasks = utils.read_tasks_from_file()
        else:
            task_max_id = db_module.get_tasks_max_id() + 1 if db_module.get_tasks_max_id() != None else 1
            task = {
                'Id': task_max_id,
                'TaskListId': request.json.get('TaskListId', ""),
                'Name': request.json.get('Name', ""),
                'Description': request.json.get('Description', ""),
                'ScheduledTime': request.json.get('ScheduledTime', ""),
                'Status': request.json.get('Status', "")
            }
            db_module.db_create_new_task(task)
        return jsonify({'task': task}), 201

    @app.route('/todo/api/user/update', methods=['POST'])
    def update_user():
        if config_value == 'Files':
            _users = utils.read_users_to_file()
            update_user = myFileUtils.update_user_in_file(request.json.get('var-to-change'),
                                                          request.json.get('new_value'),
                                                          request.json.get('Id'))
            _users = utils.read_users_to_file()
        else:
            update_user = db_module.update_user(request.json.get('Id'), request.json.get('var-to-change'),
                                                request.json.get('new_value'))
        return jsonify({"update_user": update_user}), 201

    @app.route('/todo/api/task/update', methods=['POST'])
    def update_task():
        if config_value == 'Files':
            _tasks = utils.read_tasks_from_file()
            updated_task = myFileUtils.update_task_in_file(request.json.get('var-to-change'),
                                                           request.json.get('new_value'),
                                                           request.json.get('Id'))
            _tasks = utils.read_tasks_from_file()
        else:
            updated_task = db_module.update_task(request.json.get('Id'), request.json.get('var-to-change'),
                                                 request.json.get('new_value'))
        return jsonify({"updated_task": updated_task}), 201

    @app.route('/todo/api/task-list/update', methods=['POST'])
    def update_task_list():
        if config_value == 'Files':
            _task_list = utils.read_tasks_lists_from_file()
            updated_task_list = myFileUtils.update_task_list_in_file(request.json.get('var-to-change'),
                                                                    request.json.get('new_value'),
                                                                    request.json.get('Id'))
            _task_list = utils.read_tasks_lists_from_file()
        else:
            updated_task_list = db_module.update_task_list(request.json.get('Id'), request.json.get('var-to-change'),
                                                          request.json.get('new_value'))
        return jsonify({"updated_task_list": updated_task_list}), 201

    @app.route('/todo/api/user/delete', methods=['DELETE'])
    def delete_user():
        if config_value == 'Files':
            _users = utils.read_users_to_file()
            deleted_user = myFileUtils.delete_user(request.json.get('Id'))
            _users = utils.read_users_to_file()
            user_with_id = [_user for _user in _users if
                            _user['Id'] == request.json.get('Id')]
        else:
            db_module.delete_user(request.json.get('Id'))
            _users = db_module.get_all_users()
            user_with_id = [_user for _user in _users if
                            _user['Id'] == request.json.get('Id')]
        return jsonify({"result": True if len(user_with_id) == 0 else False}), 201

    @app.route('/todo/api/task_list/delete', methods=['DELETE'])
    def delete_task_list():
        if config_value == 'Files':
            _tasksLists = utils.read_tasks_lists_from_file()
            deleted_task_list = myFileUtils.delete_task_list(request.json.get('Id'))
            _tasksLists = utils.read_tasks_lists_from_file()
            task_list_with_id = [task_list for task_list in _tasksLists if
                                 task_list['Id'] == request.json.get('Id')]
        else:
            db_module.delete_task_list(request.json.get('Id'))
            _tasksLists = db_module.get_all_task_lists()
            task_list_with_id =[task_list for task_list in _tasksLists if
                                 task_list['Id'] == request.json.get('Id')]
        return jsonify({"result": True if len(task_list_with_id) == 0 else False}), 201

    @app.route('/todo/api/task/delete', methods=['DELETE'])
    def delete_task():
        if config_value == 'Files':
            _tasks = utils.read_tasks_from_file()
            delete_task = myFileUtils.delete_task(request.json.get('Id'))
            _tasks = utils.read_tasks_from_file()
            task_with_id = [task for task in _tasks if
                            task['Id'] == request.json.get('Id')]
        else:
            db_module.delete_task(request.json.get('Id'))
            _tasks = db_module.get_all_tasks()
            task_with_id = [task for task in _tasks if
                            task['Id'] == request.json.get('Id')]
        return jsonify({"result": True if len(task_with_id) == 0 else False}), 201

    @app.errorhandler(404)
    def not_found(error):
        return make_response(jsonify({'error': 'Not found'}), 404)

    return app


if __name__ == '__main__':
    create_app().run()
    #
