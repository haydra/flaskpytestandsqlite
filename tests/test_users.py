
import json
class TestUsers(object):
    def test_create_user(self,app, client):
        data = {
            "FirstName": "dsdsd",
            "LastName": "asdsds",
            "Email": "asdasd",
            "Password": "passxcxs"
        }
        r = client.post("/todo/api/user/create", headers={'Content-Type': 'application/json'}, data=json.dumps(data))
        response_data = json.loads(r.data)
        assert response_data.get('user')['FirstName'] == data['FirstName']
        print("verifying the first-name sent and response first name are equal")
        assert response_data.get('user')['LastName'] == data['LastName']
        print("verifying the last-name sent and response last name are equal")
        assert response_data.get('user')['Email'] == data['Email']
        print("verifying the email sent and response email are equal")
        assert response_data.get('user')['Password'] == data['Password']
        print("verifying the password sent and response password are equal")
        assert 'Id' in response_data.get('user')
        print("verifying id is present in response")

    def test_update_user(self, app, client):
        data = {
            "var-to-change": "FirstName",
            "new_value": "new-first-namex",
            "Id": 3
        }
        r = client.post("/todo/api/user/update", headers={'Content-Type': 'application/json'},
                        data=json.dumps(data))
        response_data = json.loads(r.data)

        assert response_data.get('update_user')[data["var-to-change"]] == data['new_value']
        print("verifying the " + data["var-to-change"] + " sent and response " + data["var-to-change"] + " are equal")

    def test_delete_user(self, app, client):
        data = {
            "Id": 4
        }
        r = client.delete("/todo/api/user/delete", headers={'Content-Type': 'application/json'},
                          data=json.dumps(data))
        response_data = json.loads(r.data)

        assert response_data.get('result') == True
        print("verifying that record has been deleted ")
