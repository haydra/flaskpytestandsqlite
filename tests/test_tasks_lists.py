import json

class TestTaskLists(object):
    def test_create_task_list(self,app, client):
        data = {
            "UserId": 1,
            "Name": "my-task-list-2",
            "Description": "my-task-list-description"
        }
        r = client.post("/todo/api/task-list/create", headers={'Content-Type': 'application/json'},
                        data=json.dumps(data))
        response_data = json.loads(r.data)

        assert response_data.get('task_list')['UserId'] == data['UserId']
        print("verifying the user-id sent and response user-id are equal")

        assert response_data.get('task_list')['Name'] == data['Name']
        print("verifying the name sent and response name are equal")

        assert response_data.get('task_list')['Description'] == data['Description']
        print("verifying the description sent and response description are equal")

        assert 'Id' in response_data.get('task_list')
        print("verifying that id is in response data")

    def test_delete_task_list(self,app, client):
        data = {
            "Id": 12312
        }
        r = client.delete("/todo/api/task_list/delete", headers={'Content-Type': 'application/json'},
                          data=json.dumps(data))
        response_data = json.loads(r.data)

        assert response_data.get('result') == True
        print("verifying that record has been deleted ")

    def test_update_task_list(self,app, client):
        data = {
            "var-to-change": "Name",
            "new_value": "new task list value",
            "Id": 12312
        }
        r = client.post("/todo/api/task-list/update", headers={'Content-Type': 'application/json'},
                        data=json.dumps(data))
        response_data = json.loads(r.data)

        assert response_data.get('updated_task_list')[data["var-to-change"]] == data['new_value']
        print("verifying the " + data["var-to-change"] + " sent and response " + data["var-to-change"] + " are equal")
