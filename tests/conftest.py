# conftest.py
import pytest

from app.initit import create_app


@pytest.fixture(scope='session')
def app(request):
    """Session-wide test `Flask` application."""
    app = create_app()
    app.testing = True

    # Establish an application context before running the tests.
    ctx = app.app_context()
    ctx.push()

    def teardown():
        ctx.pop()
    request.addfinalizer(teardown)
    with ctx:
        yield app


@pytest.yield_fixture
def client(app):
    with app.test_client() as c:
        yield c



