import json


class TestTasks(object):
    def test_create_task(self, app, client):
        data = {
            "TaskListId": 1,
            "Name": "my-task",
            "Description": "my-task-description",
            "ScheduledTime": "26/7/16 12:55 Z",
            "Status": "open"

        }
        r = client.post("/todo/api/task/create", headers={'Content-Type': 'application/json'},
                        data=json.dumps(data))
        response_data = json.loads(r.data)

        assert response_data.get('task')['TaskListId'] == data['TaskListId']
        print("verifying the task-list-id sent and response task-list-id are equal")

        assert response_data.get('task')['Name'] == data['Name']
        print("verifying the name sent and response name are equal")

        assert response_data.get('task')['Description'] == data['Description']
        print("verifying the description sent and response description are equal")

        assert response_data.get('task')['ScheduledTime'] == data['ScheduledTime']
        print("verifying the schedule-time sent and response schedule-time are equal")

        assert response_data.get('task')['Status'] == data['Status']
        print("verifying the status sent and response status are equal")

        assert 'Id' in response_data.get('task')
        print("verifying that id is in response data")

    def test_update_task(self, app, client):
        data = {
            "var-to-change": "Description",
            "new_value": "new-description",
            "Id": 1223
        }
        r = client.post("/todo/api/task/update", headers={'Content-Type': 'application/json'},
                        data=json.dumps(data))
        response_data = json.loads(r.data)

    def test_delete_task(self, app, client):
        data = {
            "Id": 1223
        }
        r = client.delete("/todo/api/task/delete", headers={'Content-Type': 'application/json'},
                          data=json.dumps(data))
        response_data = json.loads(r.data)

        assert response_data.get('result') == True
        print("verifying that record has been deleted ")
